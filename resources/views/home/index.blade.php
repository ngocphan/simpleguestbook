@extends('layouts.main')

@section('content')
    <h1>Welcome to Simple Guest Book</h1>

    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

        <div class="col-md-5">
            <form class="form-horizontal" action="{{ url('/addGuestBook') }}" method="POST">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
                        <div class="error"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        <div class="error"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="message">Message:</label>
                    <div class="col-sm-10">
                        <textarea name="message" id="message" cols="45" rows="5"></textarea>
                        <div class="error"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

                {{ csrf_field() }}
            </form>
        </div>

        <div class="col-md-7">
            <h3>List of Messages:</h3>
                @foreach($messages as $message)
                    <div class="well well-lg">
                        Name: {{ $message->name . ' - ' . $message->email }} <br />
                        <p>Message: <i>{{ $message->message }}</i></p>
                    </div>
                @endforeach
        </div>
    </div>


@endsection

@section('bottom')
<script>
    $(document).ready(function() {
        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        };

        $(".btn").on("click", function(){
            var btn = $(this);
            var nameField = $("#name").val();

            if ( nameField == "" || nameField.length < 3) {
                $("#name").next().html("Name must not empty and must greater than 3 characters.");
                return false;
            }

            var emailField = $("#email").val();
            if (!isValidEmailAddress(emailField)) {
                $("#email").next().html("Email not valid");
                return false;
            }

            var message = $("#message").val();

            if ( message == "" || message.length < 5) {
                $("#message").next().html("Message must not be empty and must be greater than 5 characters.");
                return false;
            }

        });
    });
</script>
@endsection