<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Simple Guest Book</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/guestbook.css') }}">
    @yield('header')
</head>
<body>

    @include('layouts.partials.mainnav')

    <div class="container">
        @yield("content")
    </div>

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="bottom">
    @yield('bottom')
</div>
</body>
</html>