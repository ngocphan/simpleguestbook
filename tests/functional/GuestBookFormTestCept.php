<?php

    $I = new FunctionalTester($scenario);
    $I->wantTo('create a guestbook with message added');

    $I->amOnPage('/');
    $I->fillField(['name' => 'name'], 'Ngoc Phan');
    $I->fillField('Email:', 'ngocph06@gmail.com');
    $I->fillField('Message:', 'Have a good day');

    $I->click('Submit');
    $I->seeCurrentUrlEquals('');

    $I->seeRecord('guestbook',
      ['name' => 'Ngoc Phan',
      'email' => 'ngocph06@gmail.com']);


