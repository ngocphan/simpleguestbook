<?php

namespace App\Http\Controllers;

use App\GuestBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index()
    {
        $messages = GuestBook::orderBy('created_at', 'desc')->get();

        return view('home.index', compact('messages'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeGuestBook(Request $request)
    {
        //validate
        $this->validate($request, [
            'name'      => 'bail|required|min:3|max:255',
            'email'     => 'bail|required|email',
            'message'   => 'bail|required|min:5'
        ]);

        GuestBook::create(
            Input::only('name', 'email', 'message')
        );

        return redirect()->route('home')->with('success', 'Add message successfully.');
    }
}
