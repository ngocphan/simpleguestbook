/**
 * Created by lazhcm10343 on 2/28/17.
 */
(function ( $ )
{
    function formCheck() {
        var btn = $(this);
        var nameField = $("#name").val();

        if ( nameField == "" || nameField.length < 3) {
            $("#name").next().html("Name field must not empty and must greater than 3 characters.");
        }
    }

}(jQuery));