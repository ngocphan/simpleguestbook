<?php

    Route::get('/', [
        'as' => 'home',
        'uses' => 'HomeController@index'
    ]);

    Route::post('/addGuestBook', [
        'as' => 'add_guestbook',
        'uses' => 'HomeController@storeGuestBook'
    ]);